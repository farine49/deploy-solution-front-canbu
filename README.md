## Init your env
Run `npm install -g @angular/cli`
Run `npm install`

## Development server

Run `ng serve --port 8081` for a dev server. Navigate to `http://localhost:8081/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## DOCKER COMMANDS

docker build -t front .
docker run -p 8081:80 front
Suite du TP
Utiliser mysql.yml docker compose -f mysql.yml up
