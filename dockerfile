FROM node:16.14.0 as build
WORKDIR /usr/local/app
COPY ./ /usr/local/app
RUN npm install -g @angular/cli
RUN npm install --silent
RUN ng build --prod


# Stage 2: Serve app with nginx server

# Use official nginx image as the base image
FROM nginx:latest

# Copy the build output to replace the default nginx contents.
COPY --from=build /usr/local/app/dist/mds-dds-exam-angular /usr/share/nginx/html

# Expose port 80
EXPOSE 80